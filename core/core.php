<?php


function con(){
	include_once 'db.class.php';
	$config =  include 'config.php';
	$db = new db($config['db']);

	return $db;
	
}

/**
	格式化打印变量
**/
function dump($vars){
   $content = "<div align=left><pre>\n" . htmlspecialchars(print_r($vars, true)) . "\n</pre></div>\n";
   echo "<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"></head><body>{$content}</body></html>"; 
   return;
}

//错误提示
function err($msg=''){
	
	exit("<script>alert('".$msg."');history.go(-1);</script>");
	
}

//成功时跳转
function success($msg='',$url='',$refresh=0){
	if($refresh){
		exit("<script>alert('".$msg."');window.location.href='".$url."';location.reload();</script>");
	}else{
		exit("<script>alert('".$msg."');window.location.href='".$url."';</script>");
	}
	
}

//获取域名
function get_host(){
	if ( ! empty($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS']) !== 'off')
	{
		$protocol = "https://";
	}
	elseif (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] === 'https')
	{
		$protocol = "https://";
	}
	elseif ( ! empty($_SERVER['HTTP_FRONT_END_HTTPS']) && strtolower($_SERVER['HTTP_FRONT_END_HTTPS']) !== 'off')
	{
		$protocol = "https://";
	}else{
		$protocol = "http://";
	}
	
    if(isset($_SERVER['HTTP_X_FORWARDED_HOST'])) {
        $host = $_SERVER['HTTP_X_FORWARDED_HOST'];
    }elseif (isset($_SERVER['HTTP_HOST'])) {
        $host = $_SERVER['HTTP_HOST'];
    }else{
        if(isset($_SERVER['SERVER_PORT'])) {
            $port = ':' . $_SERVER['SERVER_PORT'];
            if((':80' == $port && 'http://' == $protocol) || (':443' == $port && 'https://' == $protocol)) {
                $port = '';
            }
        }else{
            $port = '';
        }
        if(isset($_SERVER['SERVER_NAME'])) {
            $host = $_SERVER['SERVER_NAME'].$port;
        }else if(isset($_SERVER['SERVER_ADDR'])) {
            $host = $_SERVER['SERVER_ADDR'].$port;
        }
    }
    return $protocol.$host;
}

//爬虫
function curl_get_request($url,$data=null){
	if(is_array($data)){
		$data = http_build_query($data);
	}
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, false);//不返回头部信息
	if($data!=null){
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);	
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);  //结果是否显示出来，1不显示，0显示    
	//判断是否https
	if(strpos($url,'https://')!==false){
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$UserAgent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; .NET CLR 3.5.21022; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
		curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);//设置Agent
	}
	$data = curl_exec($ch);
	curl_close($ch);
	if($data === FALSE) 
	{ 
	  $data = "curl Error:".curl_error($ch);
	} 
	return $data;
}
function curl_post_request($url,$data=null){
	if(is_array($data)){
		$data = http_build_query($data);
	}
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $url);
	curl_setopt($ch, CURLOPT_HEADER, false);//不返回头部信息
	curl_setopt($ch, CURLOPT_POST, 1);
	if($data!=null){
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);	
	}
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);  //结果是否显示出来，1不显示，0显示    
	//判断是否https
	if(strpos($url,'https://')!==false){
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		$UserAgent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; SLCC1; .NET CLR 2.0.50727; .NET CLR 3.0.04506; .NET CLR 3.5.21022; .NET CLR 1.0.3705; .NET CLR 1.1.4322)';
		curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
	}
	$data = curl_exec($ch);
	curl_close($ch);
	if($data === FALSE) 
	{ 
	  $data = "curl Error:".curl_error($ch);
	} 
	return $data;
}
function getFieldData($data='',$param=null){
	$res = preg_match_all($param,$data,$match);
	if($res){
		
		return $match[1];
		
	}else{
		return '';
	}
	
	
	
	
}


$db = con();





