<?php

include 'core.php';

if(!isset($_REQUEST['action'])){
	exit('参数错误！');
}

$action = $_REQUEST['action'];

switch($action){
	
	case 'getdatabase':
	//查询数据库里面dbconfig表

	$data = getdatabase();
	
	echo json_encode(['code'=>0,'data'=>$data]);
	exit;
	
	break;
	case 'getproject':
	
	$data = getproject();
	
	echo json_encode(['code'=>0,'data'=>$data]);
	exit;
	break;
	
	case 'createdatabase':
		
	$host = $_POST['host'] ? addslashes($_POST['host']) : err('host不能为空！');
	$port = $_POST['port'] ? addslashes($_POST['port']) : err('port不能为空！');
	$dbname = $_POST['dbname'] ? addslashes($_POST['dbname']) : err('dbname不能为空！');
	$user = $_POST['user'] ? addslashes($_POST['user']) : err('user不能为空！');
	$pass = $_POST['pass'] ? addslashes($_POST['pass']) : err('pass不能为空！');
	$addtime = time();
	$db = con();
	
	$sql="INSERT INTO `dbconfig`( `host`, `port`, `dbname`, `user`, `pass`,  `addtime`) VALUES ('".$host."','".$port."','".$dbname."','".$user."','".$pass."','".$addtime."')";
	
	$res = $db->add($sql);
	if($res){
		success('创建成功！','/database/index.html');
	}else{
		//exit('创建失败！');
		err('创建失败，请检查错误！');
	}
	
	break;
	
	case 'createproject':
	$title = $_POST['title'] ? addslashes($_POST['title']) : err('title不能为空！');
	$url = $_POST['url'] ? addslashes($_POST['url']) : err('url不能为空！');
	$description = $_POST['description'] ? addslashes($_POST['description']) : err('description不能为空！');
	$target = $_POST['target'] ? addslashes($_POST['target']) : err('target不能为空！');
	$dbid = $_POST['dbid'] ? addslashes($_POST['dbid']) : err('dbid不能为空！');
	$method = $_POST['method'] ? addslashes($_POST['method']) : err('method不能为空！');
	$addtime = time();
	$db = con();
	
	$sql="INSERT INTO `project`(`title`, `url`, `description`,  `target`, `addtime`, `dbid`) VALUES ('".$title."','".$url."','".$description."','".$target."','".$addtime."','".$dbid."')";
	
	$res = $db->add($sql);
	if($res){
		//success('创建成功！','/database/index.html');
		//处理字段的存储
		$name = $_POST['name'];
		$field = $_POST['field'];
		$param = $_POST['param'];
		foreach($name as $k=>$v){
			if($v){
				$w = [];
				$w['name'] = $v;
				$w['field'] = $field[$k];
				$w['param'] = addslashes($param[$k]);
				$w['pid'] = $res;
				$sql="INSERT INTO `fields`( `name`, `field`, `param`, `pid`) VALUES ('".$w['name']."','".$w['field']."','".$w['param']."','".$w['pid']."')";
				$r = $db->add($sql);
				if(!$r){
					err('创建字段失败！');
				}
			}
			
		}
		
		success('创建想成功！','../index.html');
		
		
	}else{
		//exit('创建失败！');
		err('创建失败，请检查错误！');
	}
	
	
	
	
	
	
	
	exit;
	
	
	break;
	
	
}



function getdatabase(){
	

	$db = con();


	$sql = "select * from dbconfig";
	$res = $db->find($sql);
	
	if(!$res){
		
		echo json_encode(['msg'=>'您还没有创建数据库，请先创建数据库！','code'=>1]);
		exit;
	}
	$new = [];
	foreach($res as $k=>$v){
		//if($k!='pass'){
		unset($v['pass']);
			$new[$k] = $v;
			$new[$k]['addtime'] = date('Y-m-d H:i:s',$v['addtime']);
	//	}
		
	}
	
	return $new;
	
	
}

function getproject(){
	

	$db = con();


	$sql = "select * from project";
	$res = $db->find($sql);
	
	if(!$res){
		
		echo json_encode(['msg'=>'您还没有创建项目，请先创建项目！','code'=>1]);
		exit;
	}
	
	foreach($res as $k=>$v){
		
		$res[$k]['addtime'] = date('Y-m-d H:i:s',$v['addtime']);
	}
	
	return $res;
	
	
}

