<?php


include_once '../core/core.php';




if(isset($_POST['go'])){
	if(!isset($_POST['id'])){
		exit('ID错误！');
	}
	$id = addslashes($_POST['id']);
	$title = $_POST['title'] ? addslashes($_POST['title']) : err('标题不能为空！');
	$url = $_POST['url'] ? addslashes($_POST['url']) : err('url不能为空！');
	$description = $_POST['description'] ? addslashes($_POST['description']) : '';
	$method = $_POST['method'] ? addslashes($_POST['method']) : 'GET';
	$target = $_POST['target'] ? addslashes($_POST['target']) : err('导入表不能为空！');
	$dbid = $_POST['dbid'] ? addslashes($_POST['dbid']) : err('选择数据库不能为空！');
	//$addtime = time();
	
	
	$sql = "UPDATE `project` SET `title`='".$title."',`url`='".$url."',`description`='".$description."',`target`='".$target."',`dbid`='".$dbid."' WHERE  id=".$id;
	
	$res = $db->update($sql);
	if(!$res){
		//err('更新失败！');
	}
	//success('更新成功！','index.html');
	
	//处理字段
	
	//直接先把原来的字段全部删掉！
	//再把现在的字段新增进去
	
	$sql =" DELETE FROM `fields` WHERE pid=".$id;
	$db->del($sql);
	$name = $_POST['name'];
	$field = $_POST['field'];
	$param = $_POST['param'];
	foreach($name as $k=>$v){
		if($v){
			$w = [];
			$w['name'] = addslashes($v);
			$w['field'] = addslashes($field[$k]);
			$w['param'] = addslashes($param[$k]);
			$w['pid'] = $id;
			$sql="INSERT INTO `fields`( `name`, `field`, `param`, `pid`) VALUES ('".$w['name']."','".$w['field']."','".$w['param']."','".$w['pid']."')";
			$r = $db->add($sql);
			if(!$r){
				err('创建字段失败！');
			}
		}
		
	}
	
	success('更新成功！',get_host().'/index.html');
	

	
}else{
	if(!isset($_GET['id'])){
		exit('链接错误！');
	}

	$id = $_GET['id'];

	$sql = "select * from project where id=".$id;

	$res = $db->find($sql);
	if(!$res){
		exit('数据库未找到，或已被删除！');
	}

	$data = $res[0];
	$sql = "select * from fields where pid=".$data['id'];

	$fields = $db->find($sql);
	

}




?>
<!DOCTYPE html>
<html lang="zh-CN">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
    <title>修改项目</title>

    <!-- Bootstrap -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim 和 Respond.js 是为了让 IE8 支持 HTML5 元素和媒体查询（media queries）功能 -->
    <!-- 警告：通过 file:// 协议（就是直接将 html 页面拖拽到浏览器中）访问页面时 Respond.js 不起作用 -->
    <!--[if lt IE 9]>
      <script src="https://cdn.jsdelivr.net/npm/html5shiv@3.7.3/dist/html5shiv.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/respond.js@1.4.2/dest/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>
	<div class="row" style="margin:20px">
    <p><h2>修改项目</h2><a href="javascript:history.go(-1)"><button type="button" class="btn btn-info">返回列表</button></a><a href="/database/add.html"><button type="button" class="btn btn-success">创建数据库</button></a></p>
	
	<form method="post" action="/project/edit.php">
	<input name="id" value="<?php echo $data['id']?>" type="hidden">
	<input name="go" value="1" type="hidden">
  <div class="form-group">
    <label for="title">项目名</label>
    <input type="text" class="form-control" value="<?php echo $data['title']?>" id="title" name="title" placeholder="项目名">
  </div>
  <div class="form-group">
    <label for="description">简介</label>
    <textarea name="description"  class="form-control"><?php echo $data['description']?></textarea>
  </div>
  <div class="form-group">
    <label for="url">目标网址</label>
    <input type="text" class="form-control" value="<?php echo $data['url']?>" id="url" name="url" placeholder="目标网址">
  </div>
   <div class="form-group">
    <label for="method">抓取方式</label>
    <select id="method" class="form-control">
	<option value="GET" <?php if($data['method']=='GET'){ echo 'selected';} ?>>GET</option>
	<option value="POST" <?php if($data['method']=='POST'){ echo 'selected';} ?>>POST</option>
	</select>
  </div>
  <div class="form-group">
    <label for="exampleInputFile">创建字段</label>
    <table class="table table-bordered">
	<tr><th>字段名</th><th>字段标识</th><th>对应规则</th><th>操作</th></tr>
	<?php 
	
	foreach($fields as $v){
		
		echo '<tr><td><input type="text" class="form-control" value="'.$v['name'].'"  name="name[]" placeholder="字段名"></td><td><input type="text" value="'.$v['field'].'"  class="form-control"  name="field[]" placeholder="字段标识"></td><td><input type="text" value="'.htmlspecialchars($v['param']).'"  class="form-control"  name="param[]" placeholder="对应规则"></td><td> <a class="addcon"><button type="button" class="btn btn-success btn-sm">新增</button></a><a class="delcon"><button type="button" class="btn btn-danger btn-sm">删除</button></a></td></tr>';
		
	}
	
	
	?>
	<tr><td><input type="text" class="form-control"  name="name[]" placeholder="字段名"></td><td><input type="text" class="form-control"  name="field[]" placeholder="字段标识"></td><td><input type="text" class="form-control"  name="param[]" placeholder="对应规则"></td><td> 
	<a class="addcon"><button type="button" class="btn btn-success btn-sm">新增</button></a>
	</td></tr>
	

	</table>
	
	
  </div>
  <div class="form-group">
    <label for="url">选择数据库</label>
    <select class="form-control" name="dbid" id="dbid">
	</select>
  </div>
 <div class="form-group">
    <label for="target">导入表</label>
    <input type="text" name="target" value="<?php echo $data['target'];?>"  class="form-control" id="target"  placeholder="导入表">
  </div>
  <button type="submit" class="btn btn-default">提交</button>
</form>
	
	
	</div>
    <!-- jQuery (Bootstrap 的所有 JavaScript 插件都依赖 jQuery，所以必须放在前边) -->
    <script src="https://cdn.jsdelivr.net/npm/jquery@1.12.4/dist/jquery.min.js"></script>
    <!-- 加载 Bootstrap 的所有 JavaScript 插件。你也可以根据需要只加载单个插件。 -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@3.3.7/dist/js/bootstrap.min.js"></script>
	<script>
	$(function(){
		$(document).on('click',".addcon",function(){
		
			var html = '<tr><td><input type="text" class="form-control"  name="name[]" placeholder="字段名"></td><td><input type="text" class="form-control"  name="field[]" placeholder="字段标识"></td><td><input type="text" class="form-control"  name="param[]" placeholder="对应规则"></td><td> <a class="addcon"><button type="button" class="btn btn-success btn-sm">新增</button></a><a class="delcon"><button type="button" class="btn btn-danger btn-sm">删除</button></a></td></tr>';
		
			$(this).parent().parent().after(html);
		
		})
		$(document).on('click',".delcon",function(){
		
			
			$(this).parent().parent().remove();
		
		})
		
		
	
	})
	
	
	function getdatabse(){
		
		$.get("/core/index.php",{action:'getdatabase'},function(res){
		console.log(res);
			if(res.code==1){
				alert(res.msg);
				//你可以选择直接跳进创建数据库里面
			}else{
				var database = '';
				var dbid = <?php echo $data['dbid'];?>;
				for(var i=0;i<res.data.length;i++){
					if(dbid==res.data[i].id){
						database+='<option value="'+res.data[i].id+'" selected="selected">'+res.data[i].dbname+'</option>';
					}else{
						database+='<option value="'+res.data[i].id+'">'+res.data[i].dbname+'</option>';
					}
					
				
				}
				$("#dbid").html(database);
				
			}
		
		},'json')
	
	}
	
	getdatabse();	
	
	</script>
	
	
	
  </body>
</html>