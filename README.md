# 简单的PHP爬虫系统

#### 介绍
简单实现创建爬虫项目，然后导入数据库的功能。

#### 软件架构
语言：PHP5.6+
数据库：MySQL
环境支持：Apache、nginx


#### 安装教程

1.  将目录里的所有文件放入服务器中，或本地项目目录中
2.  将根目录的caiji.sql导入数据库中
3. 	配置爬虫系统数据库core/config.php
4.  绑定域名到目录
5.  访问域名，进入系统

#### 使用说明

1.  需要先创建要导入的数据库
2.  创建爬虫项目，设定不同字段的爬取规则，指定数据库，指定导入表
3.  爬取字段需要跟指定表一一对应，这样才能正确导入

#### 在线演示

http://caiji.test.2wex.com

#### 参与贡献

无


#### 项目背景

该项目是在个人组织的PHP培训中最后的项目结课作业
课程已上传到B站，与项目相关视频如下：
1. https://www.bilibili.com/video/BV12K411L7wv/
2. https://www.bilibili.com/video/BV12V411o7vW/
3. https://www.bilibili.com/video/BV1ik4y1R7zX/
4. https://www.bilibili.com/video/BV1GT4y1V7AT/
5. https://www.bilibili.com/video/BV1a54y197VK/
6. https://www.bilibili.com/video/BV12T4y1G7Sf/

B站个人主页：https://space.bilibili.com/8890287

#### 关于本人

QQ：2581047041
邮箱：2581047041@qq.com
手机号：13660240863
微信：TF-2581047041

